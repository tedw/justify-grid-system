# Justify Grid System

A grid system based on the "text-align: justify" property

References:
[Justify Grid](http://justifygrid.com/)
[Text-align: Justify and RWD](http://www.barrelny.com/blog/text-align-justify-and-rwd/)

## Issues
- Requires placeholder elements (max # per row - 2)
- Fails when a grid has more single-column width elements than columns
